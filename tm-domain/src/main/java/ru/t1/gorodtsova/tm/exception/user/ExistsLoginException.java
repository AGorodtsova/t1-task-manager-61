package ru.t1.gorodtsova.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

    public ExistsLoginException(@NotNull final String login) {
        super("Error! This login '" + login + "' already exists...");
    }

}
