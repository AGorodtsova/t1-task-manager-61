package ru.t1.gorodtsova.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBase64LoadRequest;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load data from base64 file";

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataBase64LoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        domainEndpoint.loadDataBase64(request);
    }

}
